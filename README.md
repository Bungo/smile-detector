# Smile-Detecor
## Model
In order to get to know transfer learning, different models were retrained for smile detection. The code for this is in part 1 of the scripts.
## Detector
The second part uses CV2 Haarcascade to filter faces as frames from a video recording. These frames are then classified with the model from part 1. For reproducibility the smile detector in part 2 can be adjusted with the original weights "model_vgg.h5".
## Demo and documentation
The MP4 file was created as a demo to see how the final product should look like.<br>The documentation was created for the module "DeepL" during the bachelor. Therefore the documentation is German.